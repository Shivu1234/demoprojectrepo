package com.example.demo34.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo34.model.Sales;
@Repository
public interface SalesRepository extends JpaRepository<Sales, Integer> {

	List<Sales> findSalesBycustomId(int customId);

	void deleteByCustomId(int customId);

	List<Sales> findByProductId(int productId);

	void deleteByProductId(int productId);

	

	

	

	
}
