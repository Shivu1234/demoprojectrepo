package com.example.demo34.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo34.model.Profile;

public interface ProfileRepository extends JpaRepository<Profile, Integer> {

	Profile findProfileByCustomerName(String customerName);

}
