package com.example.demo34.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo34.model.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer>{

	List<Invoice> findInvoiceByCustomerId(int id);
}
