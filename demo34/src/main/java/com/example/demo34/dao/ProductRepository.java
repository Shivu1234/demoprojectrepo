package com.example.demo34.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo34.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	List<Product> findById(int productId);

	Product findByProductName(String productName);

	Optional<Product> findByProductId(int pId);

	Optional<Product> findProductByProductId(int productId);

}
