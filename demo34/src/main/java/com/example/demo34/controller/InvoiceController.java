package com.example.demo34.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo34.dao.CustomerRepository;
import com.example.demo34.dao.InvoiceRepository;
import com.example.demo34.dao.SalesRepository;
import com.example.demo34.model.Customer;
import com.example.demo34.model.Invoice;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/invoice")
public class InvoiceController {

	@Autowired
	private InvoiceRepository invRepository;
	@Autowired
	private SalesRepository salesRepository;
	@Autowired
	private CustomerRepository customerRepository;
	
	@PostMapping("/saveInvoice")
	public List<Invoice> saveInvoice(@RequestBody List<Invoice> invoice) {
		return invRepository.saveAll(invoice);
	}
	
	@GetMapping("/getAllInvoice")
	public List<Invoice> getAllInvoice(){
		return invRepository.findAll();		
	} 
	
	@GetMapping("/getInvoice/{customName}")
	public Object getInvoice(@PathVariable("customName") String customName) {
		Optional<Customer> c =customerRepository.findByCustomerName(customName);
		List<Invoice> invoice = invRepository.findInvoiceByCustomerId(c.get().getId());
		System.out.println(invoice);
		return invoice;
	}
	@PutMapping("/updateInvoice/{customName}")
	public Object updateInvoice(@PathVariable("customName") String customerName,@RequestBody List<Invoice> invoice) {
		Optional<Customer> c = customerRepository.findByCustomerName(customerName);
		List<Invoice> invo = invRepository.findInvoiceByCustomerId(c.get().getId());
		if(invo != null) {
			for(int i=0;i<invo.size();i++) {
				invo.get(i).setQty(invoice.get(i).getQty());
				//invoice.get(i).setQty(invo.get(i).getQty());
			}
			invRepository.saveAll(invoice);
		}
		System.out.println(invo);
		return invoice;
	}
	
	@DeleteMapping("/delete/{invoiceNo}")
	public void deleteInvoice(@PathVariable("invoiceNo") int invoiceNo) {
		invRepository.deleteById(invoiceNo);
	}
}
