package com.example.demo34.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo34.dao.UserRepository;
import com.example.demo34.model.User;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

	@Autowired
	private UserRepository repository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@PostMapping("/login")
	public Object userLogin(@RequestBody User user) {
		User u = repository.findUserByEmail(user.getEmail());
		// Map<String, Object> map = new HashMap<>();
		if (u != null) {
			if (passwordEncoder.matches(user.getPassword(), u.getPassword())) {
				if (u.getEmail().equals(user.getEmail())) {
					u.setLastLoginTime(new Date());				
					repository.save(u);
					return "Login Successfully.";//u.getName() + 
				}
			} else {
				return "Invalid Password.";
			}
		} else {
			return "Invalid Email";
		}
		return null;
	}

	@PostMapping("/register")
	public Object register(@RequestBody User user) {
		if (repository.existsByEmail(user.getEmail())) {
			return "this email already exists.";
		}
		if (repository.existsByPassword(user.getPassword())) {
			return "this password already exists";
		}
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setCreatedAt(new Date());
		repository.save(user);
		return "Registered Successfully.";
	}
	
	@PutMapping("/updateUser/{id}")
	public Object updateUsers(@PathVariable("id") int id ,@RequestBody User user) {
		 Optional<User> u = repository.findById(user.getId());
		 if(u.isPresent()) {
			 User u1 = u.get();
			 u1.setName(user.getName());
			 u1.setEmail(user.getEmail());
			 u1.setRole(user.getRole());
			 u1.setPassword(user.getPassword());
			 u1.setCreatedAt(user.getCreatedAt());
			 repository.save(u1);
		 }
		return "Updated Successfully.";
		 
	}

	@GetMapping("/getAllUsers")
	public List<User> findAllUsers() {
		return repository.findAll();
	}

	@GetMapping("/findUser/{email}")
	public List<User> findUser(@PathVariable String email) {
		return repository.findByEmail(email);
	}

	@GetMapping("/getUser/{id}")
	public Optional<User> getUserById(@PathVariable int id) {
		return repository.findUserById(id);	
	}
	@DeleteMapping("/cancel/{id}")
	public List<User> cancelRegistration(@PathVariable int id) {
		repository.deleteById(id);
		return repository.findAll();
	}
}
