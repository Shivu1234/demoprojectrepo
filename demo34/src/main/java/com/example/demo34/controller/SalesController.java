package com.example.demo34.controller;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo34.dao.CustomerRepository;
import com.example.demo34.dao.ProductRepository;
import com.example.demo34.dao.ProfileRepository;
import com.example.demo34.dao.SalesRepository;
import com.example.demo34.model.Customer;
import com.example.demo34.model.Product;
import com.example.demo34.model.Profile;
import com.example.demo34.model.Sales;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/sales")
@Transactional
public class SalesController {

	@Autowired
	private SalesRepository salesRepository;
	@Autowired
	private ProfileRepository profileRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private ProductRepository productRepository;
	
	@PostMapping("/addSales")
	public Object createSalesReport(@RequestBody List<Sales> sales) {
		for(int i=0; i<sales.size(); i++) {
			Random rnd = new Random();
			int n = 100000 + rnd.nextInt(900000);
			System.out.println(n);
			sales.get(i).setHsn(n);
		}
//		for(int i=0; i<sales.size(); i++) {
//			System.out.println("cId: "+sales.get(i).getCustomId());
//			System.out.println("pId: "+sales.get(i).getProductId());
//			System.out.println("=======================================");
//			
//		}
		return salesRepository.saveAll(sales);
		
	}
	
	@GetMapping("/getAllSalesRecord")
	public List<Sales> getAllSalesRecord(){
		return salesRepository.findAll();	
	}
	
	@PutMapping("/updateSales/{customId}")
	public Object updateSales(@PathVariable("customId") int customId, @RequestBody List<Sales> sales) {
	    List<Sales> sl = salesRepository.findSalesBycustomId(customId);
		if(sl != null) {
	    for(int i=0; i<sales.size(); i++) {
	    List<Sales>	pr = salesRepository.findByProductId(sl.get(i).getProductId());
			sl.get(i).setPayment(sales.get(i).getPayment());
			sl.get(i).setLeadClosedBy(sales.get(i).getLeadClosedBy());
			salesRepository.saveAll(sl);
		  }
		}
		return sl;		
	}
	
	@GetMapping("/findSalesRecord/{customId}")
	public Object getSalesByCId(@PathVariable("customId") int customId) {
		List<Sales> sl = salesRepository.findSalesBycustomId(customId);
		System.out.println(sl);
		return sl;
	}
	
	@GetMapping("/findSalesByName/{customName}")
	public Object getSalesByCustomerName(@PathVariable("customName") String customName) {
		Customer cr = customerRepository.findSalesBycustomerName(customName);
		List<Sales> sl = salesRepository.findSalesBycustomId(cr.getId());
		
//		Profile profile = profileRepository.findProfileByCustomerName(customName);
//		System.out.println(profile.getInvoicePrefix());
//		sl.get(0).setInvoiceNum(profile.getInvoicePrefix());
//		
//		for(int i=0;i<sl.size();i++) {
//			Optional<Product> p = productRepository.findProductByProductId(sl.get(i).getProductId());
//			System.out.println(p.get().getTax());
//			int nums = Integer.parseInt(p.get().getTax().replaceAll("[^0-9 ]", "").replaceAll(" +", " ").trim());
//			int n = (sl.get(i).getFinalizeAmount()) * nums/100;
//			int n1 = n + (sl.get(i).getFinalizeAmount());
//			System.out.println(n);
//			System.out.println(n1);
//			sl.get(i).setTax(p.get().getTax());
//			sl.get(i).setAmount(n1);
//          }
		return sl;
	}
	
	@GetMapping("/getAllSalesRecord/{id}")
	public Optional<Sales> getAllSalesById(@PathVariable("id") int id){
		return salesRepository.findById(id);
	}
	
	@DeleteMapping("/delete/{customId}")
	public void deleteSalesById(@PathVariable("customId") int customId) {
		 salesRepository.deleteByCustomId(customId);
	}
	
	@DeleteMapping("/deleteSale/{productId}")
	public void deleteSalesByProductId(@PathVariable("productId") int productId) {
		salesRepository.deleteByProductId(productId);
	}
}
