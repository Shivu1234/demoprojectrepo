package com.example.demo34.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo34.dao.CustomerRepository;
import com.example.demo34.dao.ProductRepository;
import com.example.demo34.dao.ProfileRepository;
import com.example.demo34.dao.SalesRepository;
import com.example.demo34.exception.ResourceNotFoundException;
import com.example.demo34.model.Customer;
import com.example.demo34.model.Product;
import com.example.demo34.model.Profile;
import com.example.demo34.model.Sales;

@RestController
@RequestMapping("/customer")
@CrossOrigin(origins = "http://localhost:4200")
@Transactional
public class CustomerController {

	@Autowired
	private CustomerRepository repository;
	@Autowired
	private ProfileRepository profileRepository;
	@Autowired
	private ProductRepository productRep;
	
	@PostMapping("/customers")
	public Object addCustomer(@RequestBody Customer customer) {
		if (repository.existsByEmail(customer.getEmail())) {
			return "this email already exists.";
		}
		customer.setCreatedAt(new Date());
//		Profile p = profileRepository.findProfileByCustomerName(customer.getCustomerName());
//		String nums = (p.getInvoicePrefix().replaceAll("[^0-9 ]", "").replaceAll(" +", " ").trim());
//		customer.setInvoicePrefix(nums);
//		System.out.println(nums);
		repository.save(customer);
		return "Customer added successfully.";
	}

	@GetMapping("/customers")
	public List<Customer> getAllCustomers() {
		return repository.findAll();
	}

	@GetMapping("/customers/{id}")
	public Object getCustomerById(@PathVariable(value = "id") int id) {
		Optional<Customer> customer = repository.findById(id);
		if (customer.isPresent()) {
			return repository.findById(id);
		} else {
			return "Customer not found for this id : " + id;
		}
	}

	@GetMapping("/customerName/{customerName}")
	public Object getCustomersByCustomerName(@PathVariable("customerName") String customerName) {
		Optional<Customer> c = repository.findByCustomerName(customerName);
		if(c.isPresent()) {
		String nums = (c.get().getInvoicePrefix().replaceAll("[^0-9 ]", "").replaceAll(" +", " ").trim());
		c.get().setInvoicePrefix(nums);}
		return repository.findByCustomerName(customerName);
	}

	@GetMapping("/customer/{mobileNo}")
	public Optional<Customer> findCustomersByMobileNo(@PathVariable("mobileNo") String mobileNo) {
		Optional<Customer> c = repository.findByMobileNo(mobileNo);
		System.out.println(c);
		return c;
	}
	@GetMapping("/findCustomer/{email}")
	public List<Customer> findCustomerByEmail(@PathVariable("email") String email) {
		return repository.findByEmail(email);
	}

	@PutMapping("/customers/{id}")
	public Object updateCustomer(@PathVariable(value = "id") int id, @RequestBody Customer customerDetails) {
		try{
		Optional<Customer> c = repository.findById(id);
//		System.out.println(c.isPresent());
		if (c.isPresent()) {
			Customer customer = c.get();
			customer.setCustomerName(customerDetails.getCustomerName());
			customer.setContactPersonName(customerDetails.getContactPersonName());
			customer.setEmail(customerDetails.getEmail());
			customer.setMobileNo(customerDetails.getMobileNo());
			customer.setState(customerDetails.getState());
			customer.setCity(customerDetails.getCity());
			customer.setVillage(customerDetails.getVillage());
			customer.setCustomerGstIn(customerDetails.getCustomerGstIn());
			customer.setProductName(customerDetails.getProductName());
			List<Product> proList = new ArrayList<Product>();
			for(Product p : customerDetails.getProducts()) {
				proList.add(productRep.findById(p.getProductId()).get(0));
			}
			customer.setProducts(proList);
			repository.save(customer);
			return "Customer updated successfully.";
		} else {
			return "Customer not found for this id : " + id;
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "Customer not found for this id : " + id;
	}

	@DeleteMapping("/customers/{id}")
	public Map<String, Boolean> deleteCustomer(@PathVariable(value = "id") int id) throws ResourceNotFoundException {
		Customer customer = repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Customer not found for this id :: " + id));

		repository.delete(customer);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
