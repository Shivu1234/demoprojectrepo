package com.example.demo34.controller;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo34.dao.ProductRepository;
import com.example.demo34.model.Product;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "http://localhost:4200")
@Transactional
public class ProductController {

	@Autowired
	private ProductRepository productRepository;

	@PostMapping("/addProduct")
	public Object addProduct(@RequestBody Product product) {

			Random rnd = new Random();
			int n = 100000 + rnd.nextInt(900000);
			System.out.println(n);
			product.setHsn(n);
		
		productRepository.save(product);
		return "Product Added Successfully.";
	}

	@GetMapping("/getAllProducts")
	public List<Product> getAllProducts() {
		return productRepository.findAll();
	}

	@GetMapping("/getAllProducts/{id}")
	public List<Product> getProductById(@PathVariable("id") int productId) {
		return productRepository.findById(productId);
	}

	@PutMapping("/updateProduct/{id}")
	public Object updateProduct(@PathVariable("id") int productId, @RequestBody Product products) {
		Optional<Product> p = productRepository.findByProductId(products.getProductId());
		if(p.isPresent()) {
			Product pp = p.get();
			pp.setProductName(products.getProductName());
			pp.setManufacturingDate(products.getManufacturingDate());
			pp.setDescription(products.getDescription());
			productRepository.save(pp);
		}
		return null;
		
	}
	@DeleteMapping("/deleteProduct/{id}")
	public List<Product> deleteProductById(@PathVariable("id") int id) {
		productRepository.deleteById(id);
		return productRepository.findAll();
	}
}
