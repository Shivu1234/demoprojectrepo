package com.example.demo34.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo34.dao.CustomerRepository;
import com.example.demo34.dao.ProfileRepository;
import com.example.demo34.model.Customer;
import com.example.demo34.model.Profile;

@RestController
@RequestMapping("/profile")
@CrossOrigin(origins = "http://localhost:4200")
public class ProfileController {
	
	@Autowired
	private ProfileRepository profileRepository;
	@Autowired
	private CustomerRepository customerRepository;
	
	@PostMapping("/saveProfile")
	public Object saveProfile(@RequestBody Profile profile) {
		Optional<Customer> c = customerRepository.findByCustomerName(profile.getCustomerName());
		if(c.get().getCustomerName().equals(profile.getCustomerName())) {
			c.get().setInvoicePrefix(profile.getInvoicePrefix());
			profileRepository.save(profile);
			return "Profile saved successfully!.";
		}else {
			return "Customer name does not exists.";
		}	
	}
	
	@GetMapping("/getAllProfile")
	public List<Profile> getAllProfile(){
		return profileRepository.findAll();
	}
}
