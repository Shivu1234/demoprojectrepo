package com.example.demo34.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "t_customer")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String customerName;
	private String contactPersonName;
	private String mobileNo;
	private String email;
	private String village;
	private String state;
	private String city;
	private Date createdAt;
	private String productName;
	private int customerGstIn;
	private String invoicePrefix; 
	
	
	@ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(
        name = "Customer_Product", 
        joinColumns = { @JoinColumn(name = "id") }, 
        inverseJoinColumns = { @JoinColumn(name = "productId") }
    )
	private List<Product> products;
	
	
}
