package com.example.demo34.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Sales {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int customId;
	private int productId;
	private String productName;
	private String dateOfPurches;
	private int finalizeAmount;
	private String advance;
	private String payment;
	private String leadClosedBy;
	private int qty;
	private String tax;
	private int amount;
	private int hsn;
	private int customerGstIn;
	
	
//	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
//	@JoinColumn(name = "p_id")
//	private Product products;
//		
//	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
//	@JoinColumn(name = "c_id")
//	private Customer customers;
}
