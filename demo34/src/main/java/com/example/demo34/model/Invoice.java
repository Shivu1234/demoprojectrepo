package com.example.demo34.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "invoice")
public class Invoice {

	@Id
	@GeneratedValue
	private int invoiceNo;
	private int customerId;
	private String productName;
	private int hsn;
	private int qty;
	private int finalizeAmount;
	private int amount;
//	private int sub_total;
//	private int cgst;
//	private int sgst;
//	private int igst;
//	private int totalAmt;
	
}
