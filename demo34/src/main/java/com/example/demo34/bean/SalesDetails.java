package com.example.demo34.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesDetails {

	//private int id;
	private int cId;
	private int pId;
	private String dateOfPurches;
	private float finalizeAmount;
	private float advance;
	private String leadClosedBy;
}
